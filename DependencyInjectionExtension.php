<?php namespace Decoupled\Core\Extension\DependencyInjection;

use Decoupled\Core\Application\ApplicationExtension;
use Decoupled\Core\DependencyInjection\DependencyInjectionInvoker;
use Decoupled\Core\Inflector\Inflector;
use Symfony\Component\ClassLoader\Psr4ClassLoader;

class DependencyInjectionExtension extends ApplicationExtension{

    public function getName()
    {
        return 'dependency.injection.extension';
    }

    public function extend()
    {
        $app = $this->getApp();

        $app['$inflector'] = function(){

            return new Inflector();
        };

        $app['$dependency.injection.loader'] = function(){

            return new Psr4ClassLoader();
        };

        $app['$dependency.injection.invoker'] = function( $app ){

            $dii = new DependencyInjectionInvoker();

            $dii->setInflector( $app['$inflector'] );

            $dii->setApp( $app );

            return $dii;
        };

        $app['$app'] = function( $app ){

            return $app;
        };

        $app->extend('$action.factory', function( $factory, $app ){

            return $factory->setInvoker( 
                $app['$dependency.injection.invoker']
            );
        });
    }

}