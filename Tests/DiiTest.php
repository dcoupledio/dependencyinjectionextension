<?php 

require '../vendor/autoload.php';

use phpunit\framework\TestCase;
use Decoupled\Core\Extension\DependencyInjection\DependencyInjectionExtension;
use Decoupled\Core\Extension\Action\ActionExtension;
use Decoupled\Core\Application\ApplicationContainer;
use Decoupled\Core\Application\Application;


class DiiTest extends TestCase{

    public function testCanInjectAction()
    {
        $app = new Application( new ApplicationContainer() );

        $app->uses( new ActionExtension() );

        $app->uses( new DependencyInjectionExtension() );

        //create dummy services 
        $app->uses(function(){

            $this['example.service'] = function(){ return 1; };
        });

        //inject the dummy service
        $app->uses(function( $exampleService ){

            $this['example.service.2'] = function() use($exampleService){
                return $exampleService;
            };
        });

        $this->assertEquals( $app['example.service'], $app['example.service.2'] );

        $this->assertInstanceOf( ApplicationContainer::class, $app['$app'] );
    }
}